<?php
echo "</br>задача 4: подзадача 1</br>";
$number=rand(-1000,1000);
$number1=$number;
if($number>0){
    $number++;
}
echo $number;
echo "</br>задача 4: подзадача 2</br>";
$number=rand(-1000,1000);
$number2=$number;
if($number>0){
    $number++;
}
else if($number<0){
    $number-=2;
}
else{
    $number=10;
}
echo $number;
echo "</br>задача 4: подзадача 3</br>";
$numbers=array();
for($i=0;$i<10;$i++){
    $numbers[]=rand(-1000,1000);
}
$i=0;
foreach ($numbers as $number){
    if($number>0){
        $i++;
    }
}
echo $i;
echo "</br>задача 4: подзадача 4</br>";
$A=rand(-1000,1000);
$B=rand(-1000,1000);
if($A>$B){
    $B=$A;
}
else if($B>$A){
    $A=$B;
}
else{
    $A=0;
    $B=0;
}
echo '$A='.$A.' $B='.$B;
echo "</br>задача 4: подзадача 5</br>";
$month=rand(1,12);
switch($month){
    case 1:
        echo "Январь";
        break;
    case 2:
        echo "Февраль";
        break;
    case 3:
        echo "Март";
        break;
    case 4:
        echo "Апрель";
        break;
    case 5:
        echo "Май";
        break;
    case 6:
        echo "Июнь";
        break;
    case 7:
        echo "Июль";
        break;
    case 8:
        echo "Август";
        break;
    case 9:
        echo "Сентябрь";
        break;
    case 10:
        echo "Октябрь";
        break;
    case 11:
        echo "Ноябрь";
        break;
    case 12:
        echo "Декабрь";
        break;
    default:
        break;
}
echo "</br>задача 4: подзадача 6</br>";
$A=rand(-1000,1000);
$B=rand(-1000,1000);
$action=rand(1,4);
switch($action){
    case 1:
        echo $A."+".$B."=".($A+$B);
        break;
    case 2:
        echo $A."-".$B."=".($A-$B);
        break;
    case 3:
        echo $A."*".$B."=".($A*$B);
        break;
    case 4:
        echo $A."/".$B."=".($A/$B);
        break;
    default:
        break;
}
echo "</br>задача 4: подзадача 7</br>";
$items=['кошка',
    'собака',
    'крокодил',
    'слон',
    'бобр',
    'конь',
    'выхухоль',
    'стол',
    'стул',
    'диван',
    'кровать',
    'кресло',
    'бра',
    'ящик'];
$item=$items[rand(0,count($items))];
$animals=['кошка',
    'собака',
    'крокодил',
    'слон',
    'бобр',
    'конь',
    'выхухоль'];
$furniture=['стол',
    'стул',
    'диван',
    'кровать',
    'кресло',
    'бра'];
if(array_search($item,$animals)!==false){
    echo $item." is animal";
}
else if(array_search($item,$furniture)!==false){
    echo $item." is furniture";
}
else{
    echo $item." is unknown";
}
echo "</br>задача 4: подзадача 8</br>";
$number1>0?$number1++:$number1;
echo "from task 4.1 ".$number1."</br>";
$number2>0?$number2++:($number2<0?$number2-=2:$number2=10);
echo "from task 4.2 ".$number2."</br>";