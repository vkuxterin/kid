<?php
function pow($number,$power){
    return $number**$power;
}
function array_sum(array $array,bool $isReturnInt=false){
    $sum=0;
    foreach ($array as $value){
        $sum+=$value;
    }
    if($isReturnInt){
        return (int)$sum;
    }
    else{
        return (string)$sum;
    }
}
function killDog(string $data){
    $dog=$data;
    str_replace('@','',$dog);
    return $dog;
}
function revertString(string $data){
    $dog=$data;
    if(is_string($data)){
        return false;
    }
    else if(strpos($data,'@')){
        $dog=killDog($data);
    }
    return strrev($dog);
}