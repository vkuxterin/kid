<?php
class user{
    private $id;
    private $name;
    private $surname;
    public function __construct($id,$name,$surname){
        $this->id=$id;
        $this->name=$name;
        $this->surname=$surname;
    }
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }
    public function getPosts():array {
        global $posts;
        $user_posts=[];
        foreach ($posts as $post){
            if($post->getUserId()==$this->id){
                $user_posts[]=$post;
            }
        }
        return $user_posts;
    }
    public function getLikes():array {
        global $likes;
        $likes_posts=[];
        foreach ($likes as $like){
            if($like->getUserId()==$this->id){
                $likes_posts[]=$like;
            }
        }
        return $likes_posts;
    }
    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getSurname(){
        return $this->surname;
    }
    public function setSurname($surname){
        $this->surname = $surname;
    }
}