<?php
class post{
    private $id;
    private $title;
    private $user_id;
    public function __construct($id,$user_id,$title){
        $this->id=$id;
        $this->user_id=$user_id;
        $this->title=$title;
    }
    public function getUserId()
    {
        return $this->user_id;
    }
    public function getUser(){
        global $users;
        return $users[$this->user_id];
    }
    public function getId(){
        return $this->id;
    }
    public function getTitle(){
        return $this->title;
    }
}