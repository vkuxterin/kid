<?php
class like{
    private $id;
    private $user_id;
    private $post_id;
    public function __construct($id,$user_id,$post_id){
        $this->id=$id;
        $this->user_id=$user_id;
        $this->post_id=$post_id;
    }
    public function getPost(){
        global $posts;
        return $posts[$this->post_id];
    }
    public function getUser(){
        global $users;
        return $users[$this->user_id];
    }
    public function getUserID(){
        return $this->user_id;
    }
}