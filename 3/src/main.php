<?php
require_once "post.php";
require_once "user.php";
require_once "like.php";
const POST_PRICE=0.5;
const LIKE_PRICE=1;

$json_likes=@file_get_contents('likes.json');
$json_posts=@file_get_contents('posts.json');
$json_users=@file_get_contents('users.json');
$object_json=json_decode($json_users);
$users=[];
foreach ($object_json->{'users'} as $user_json){
    $users[$user_json->{'id'}]=new user($user_json->{'id'},$user_json->{'name'},$user_json->{'surname'});
}
$object_json=json_decode($json_posts);
$posts=[];
foreach ($object_json->{'posts'} as $post_json){
    $posts[$post_json->{'id'}]=new post($post_json->{'id'},$post_json->{'user_id'},$post_json->{'title'});
}
$object_json=json_decode($json_likes);
$likes=[];
foreach ($object_json->{'posts'} as $like_json){
    $likes[$like_json->{'id'}]=new like($like_json->{'id'},$like_json->{'user_id'},$like_json->{'post_id'});
}
echo "Рейтинг пользователей";
echo "<table border='1'><tr><td>Имя</td><td>Фамилия</td><td>Рейтинг</td></tr>";
foreach ($users as $user){
    echo "<tr><td>".$user->getName().'</td><td>'.$user->getSurName().'</td>';
    $user_posts=$user->getPosts();
    $rating=POST_PRICE*count($user_posts);
    $user_likes=$user->getLikes();
    $user_post_likes=0;
    foreach ($user_likes as $like){
        if(!($like->getPost()->getUserId()==$user->getId())){
            $user_post_likes++;
        }
    }
    $rating+=$user_post_likes*LIKE_PRICE;
    echo "<td>".$rating."</td></tr>";
}
echo "</table>";
