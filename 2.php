<?php
echo "задача 2: подзадача 1</br>";
echo "Одинарные кавычки</br>";
echo 'I will do this task tomorrow.</br>I\'ve already done this task.</br>The keyboard costs $15.25.</br>I haven\'t read "Hamlet" yet.</br>';
echo "Двойные кавычки</br>";
echo "I will do this task tomorrow.</br>I've already done this task.</br>The keyboard costs $15.25.</br>I haven't read \"Hamlet\" yet.</br>";
echo "</br>задача 2: подзадача 2</br>";
$cost='30';
echo "The keyboard costs $".$cost;
echo "</br>задача 2: подзадача 3</br>";
echo "</br>У лукоморья дуб зелёный;</br>Златая цепь на дубе том:</br>И днём и ночью кот учёный</br>Всё ходит по цепи кругом;";
echo "</br>задача 2: подзадача 4</br>";
$id=10;
$type='article';
echo "<a href=index.php?id=".$id."&type=".$type.">Ссылка</a></br>";
echo "</br>задача 2: подзадача 5</br>";
$strings=array(
    'example@text.com',

    'test.example',

    '@my_account',

    "test\0x40"
);
for($i=0;$i<4;$i++){
    if(strripos($strings[$i],'@')===false){
        echo $strings[$i]." Не содержит символа @";
    }
    else{
        echo $strings[$i]." Содержит символ @";
    }
    echo "</br>";
}
echo "</br>задача 2: подзадача 6</br>";
$strings=array(
    "I like an apple." ,
    "Apple is a fruit."
);
$apples=array(
    "apple",
    "Apple"
);
$oranges=array(
    "orange",
    "Orange"
);
for($i=0;$i<2;$i++) {
    $str=str_replace($apples,$oranges,$strings[$i]);
    echo $str."</br>";
}
echo "</br>задача 2: подзадача 7</br>";
$results=10;
$patterns=array(
    "Показано %results% результатов на странице.",
    "Found %results% results per page."
);
echo str_replace("%results%",$results,$patterns[0])."</br>";
echo str_replace("%results%",$results,$patterns[1])."</br>";
echo "</br>задача 2: подзадача 8</br>";
$strings=array(
    "Apple",
    "Hello, World!",
    "Здравствуй, мир!",
    "实识食石"
);
for($i=0;$i<4;$i++){
    $strlen_result=strlen($strings[$i]);
    //$mb_strlen_result=mb_strlen($strings[$i]);
    echo $strlen_result."</br>";
    //echo $mb_strlen_result."</br>";
}