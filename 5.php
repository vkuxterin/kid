<?php
echo "<a href='5.php?show=1'>Вывод больших кусков текста</a></br>";
echo "</br>задача 5: подзадача 1a</br>";
echo "цикл do-while выполняет последовательность операций, определенных в своём теле, как минимум один раз, вне зависимости от условий</br>";
echo "</br>задача 5: подзадача 1b</br>";
echo "цикл foreach не позволяет создавать бесконечный цикл";
echo "</br>задача 5: подзадача 1c</br>";
echo "<a href='infinite_loop_for.php'>Бесконечный цикл for</a></br>";
echo "<a href='infinite_loop_while.php'>Бесконечный цикл while</a><br>";
echo "<a href='infinite_loop_doWhile.php'>Бесконечный цикл do-while</a></br>";
echo "</br>задача 5: подзадача 2</br>";
$message="Noveo. Internship - 2017";
$a = [];
for($i=0;$i<10;$i++){
    $a[]=$message;
}
echo "<a href='5.php?ctype=1'>вывод сообщения с помощью цикла for</a></br>";
echo "<a href='5.php?ctype=2'>вывод сообщения с помощью цикла foreach</a></br>";
echo "<a href='5.php?ctype=3'>вывод сообщения с помощью цикла while</a></br>";
echo "<a href='5.php?ctype=4'>вывод сообщения с помощью цикла do while</a></br>";
$ctype=$_GET['ctype'];
switch($ctype) {
    case 1:
        for ($i = 0;$i<10;$i++) {
            echo $message."</br>";
            $a[] = $message;
        }
        break;
    case 2:
        foreach ($a as $msg) {
            echo $msg . "</br>";
        }
        break;
    case 3:
        $i = 0;
        while ($i < 10) {
            echo $message . "</br>";
            $i++;
        }
        break;
    case 4:
        $i = 0;
        do {
            echo $message . "</br>";
            $i++;
        } while ($i < 10);
        break;
    default:
        break;
}
echo "</br>задача 5: подзадача 3</br>";
function sortArray(&$a){
    $left=0;
    $right=count($a)-1;
    while($left<=$right){
        for($i=$left;$i<$right;$i++){
            if($a[$i]>$a[$i+1]){
                $tmp=$a[$i];
                $a[$i]=$a[$i+1];
                $a[$i+1]=$tmp;
            }
        }
        $right--;
        for($i=$right;$i>$right;$i--){
            if($a[$i]<$a[$i-1]){
                $tmp=$a[$i];
                $a[$i]=$a[$i-1];
                $a[$i-1]=$tmp;
            }
        }
        $left++;
    }
}
$array_size=rand(10,50);
$array=[];
for($i=0;$i<$array_size;$i++){
    $array[]=rand(0,1000);
}
print_r($array);
sortArray($array);
print_r($array);
echo "</br>задача 5: подзадача 4</br>";
$fib1=1;
$fib2=1;
switch($ctype) {
    case 1:
        for ($summ = $fib1+$fib2;$summ<610;$summ+=$fib_sum) {
            $fib_sum=$fib1+$fib2;
            $fib1=$fib2;
            $fib2=$fib_sum;
        }
        break;
    case 3:
        $fib_sum=$fib1+$fib2;
        $summ=$fib1+$fib2;
        while($fib_sum<610){
            $fib_sum=$fib1+$fib2;
            $fib1=$fib2;
            $fib2=$fib_sum;
            $summ+=$fib_sum;
        }
        break;
    case 4:
        $i = 0;
        do {
            $fib_sum=$fib1+$fib2;
            $fib1=$fib2;
            $fib2=$fib_sum;
        } while ($summ < 610);
        break;
    default:
        break;
}
echo $summ."</br>";
echo "</br>задача 5: подзадача 5</br>";
echo "таблица умножения";
echo "<table border='1'>";
for($i=0;$i<10;$i++){
    echo "<tr>";
    for($j=0;$j<10;$j++){
        if($i==0&&$j!=0){
            echo "<td>".$j;
        }
        else if($j==0&&$i!=0){
            echo "<td>".$i;
        }
        else {
            echo "<td>" . ($i * $j);
        }
        echo "</td>";
    }
    echo "</tr>";
}
echo "</table>";
echo "таблица сложения";
echo "<table border='1'>";
for($i=0;$i<10;$i++){
    echo "<tr>";
    for($j=0;$j<10;$j++){
        echo "<td>" . ($i + $j)."</td>";
    }
    echo "</tr>";
}
echo "</table>";
echo "</br>задача 5: подзадача 6</br>";
if($_GET['show']==1) {
    for ($i = 1; $i <= 10000; $i++) {
        if ($i % 5 != '0') {
            if (strrchr(strval($i), '3') !== false) {
                echo $i . " ";
            }
        }
    }
}
echo "</br>задача 5: подзадача 7</br>";
$way=0;
$step=0;
$back=0;
$time=0.0;
while($way<=100){
    $step++;
    $time+=1.5;
    if($step%3==0){
        $back++;
        $way--;
    }
    else{
        $way++;
    }
}
echo "Время потребовалось : ".$time."</br>Процент шагов сделанных назад : ".((100*$back)/$step)."</br>";
echo "</br>задача 5: подзадача 8</br>";
$lucky=0;
$billet=0;
if($_GET['show']==1){

    for($i=0;$i<1000;$i++){
        for($j=0;$j<1000;$j++){
            $billet++;
            $a3=$i%10;
            $a2=($i%100-$a3)/10;
            $a1=($i-$a2*10-$a3)/100;
            $b3=$j%10;
            $b2=($j%100-$b3)/10;
            $b1=($j-$b2*10-$b3)/100;
            if($a1+$a2+$a3==$b1+$b2+$b3){
                echo $a1.$a2.$a3.$b1.$b2.$b3.' ';
                $lucky++;
            }
        }
    }
}
for($i=0;$i<1000;$i++){
    for($j=0;$j<1000;$j++){
        $billet++;
        $a3=$i%10;
        $a2=($i%100-$a3)/10;
        $a1=($i-$a2*10-$a3)/100;
        $b3=$j%10;
        $b2=($j%100-$b3)/10;
        $b1=($j-$b2*10-$b3)/100;
        if($a1+$a2+$a3==$b1+$b2+$b3){
            $lucky++;
        }
    }
}
echo "</br>Процент счастливых билетов : ".((100*$lucky)/$billet);