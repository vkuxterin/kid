<?php
#task 1
#subtask 1
function testNumber($a){
    if(is_numeric($a)){
        echo $a." - is a number";
    }
    else{
        echo $a." - isn't a number";
    }
    echo "</br>";
}
echo "задача 1: подзадача 1</br>";
$a=array(8,'8','08',10.8,'11,8','-2,5','two', '4floor', 0xDEBFC,'10e20');
for($i=0;$i<10;$i++){
    testNumber($a[$i]);
}
#subtask 2
echo "задача 1: подзадача 2</br>";
$b=array(2.4,2.5,-2.5);
for($i=0;$i<3;$i++){
    echo "число ".$b[$i]." ";
    echo round($b[$i],0,PHP_ROUND_HALF_DOWN)." ";
    echo round($b[$i],0,PHP_ROUND_HALF_UP)." </br>";
}
#subtask 3
echo "задача 1: подзадача 3</br>";
$price=145.09;
$discount=5;
echo round($price*((100-$discount)/100),2)."</br>";
#subtask 4
echo "задача 1: подзадача 4</br>";
$num1=rand(1,10000)/100;
$num2=rand(1,10000)/100;
echo $num1." ".$num2."</br>";
echo "Сумма ".($num1+$num2)."</br>";
echo "Разность ".($num1-$num2)."</br>";
echo "Произведение ".($num1*$num2)."</br>";
echo "Частное ".($num1/$num2)."</br>";
echo "Среднее значение ".(($num1+$num2)/2)."</br>";
echo "Возведение в степень ".pow($num1,$num2)."</br>";
#subtask 5
echo "задача 1: подзадача 5</br>";
$lenght=5;
$square=$lenght*$lenght;
echo "Площадь квадрата:".($square)."</br>";
echo "Длина диагонали квадрата:".(sqrt($square+$square))."</br>";
echo "Длина окружности:".(2*M_PI*$lenght)."</br>";
echo "Площадь круга:".(M_PI*$square)."</br>";