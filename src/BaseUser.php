<?php
require_once "BaseUserInterface.php";
class BaseUser implements BaseUserInterface {
    private $username;
    private $password;
    function getUserName(){
        return $this->username;
    }
    function setUsername($username){
        $this->username=$username;
    }
    function getPassword(){
        return $this->password;
    }
    function setPassword($password){
        $this->password=$password;
    }
}