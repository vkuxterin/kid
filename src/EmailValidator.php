<?php
require_once "ValidatorInterface.php";
class EmailValidator implements ValidatorInterface {
    const REGEXP="/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/";
    static function Validate(string $value)
    {
        if(preg_match(self::REGEXP,$value)!=1){
            throw new Exception("Invalid email: ".$value);
        }
    }

}