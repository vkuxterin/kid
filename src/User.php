<?php
require_once "BaseUser.php";
class user extends BaseUser{
    private $email;
    public function getEmail(){
        return $this->email;
    }
    public function setEmail($email)    {
        $this->email = $email;
    }
    public function validate(){
        EmailValidator::Validate($this->email);
    }
}