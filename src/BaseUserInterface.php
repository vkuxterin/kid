<?php
interface BaseUserInterface{
    function getUserName();
    function setUsername($username);
    function getPassword();
    function setPassword($password);
}