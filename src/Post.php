<?php
require_once "BaseUserInterface.php";
require_once "Collection.php";
require_once "UserAwareInterface.php";
require_once "UserAwareTrait.php";
class Post implements UserAwareInterface{
    use UserAwareTrait;
    const STATUS_DRAFT = 1;//1e
    const STATUS_PUBLISHED = 2;
    private $title = 'Post title';//1a
    private $date;
    private $content = 'Some content';//1b
    private $status = self::STATUS_DRAFT;
    private static $statusLabels = [self::STATUS_DRAFT => 'draft', self::STATUS_PUBLISHED => 'published'];
    private $comments;

    function Post()
    {
        settype($this->date, 'DateTime');//1c
        $this->date = new DateTime();//1d
        $this->comments = new Collection();
    }

    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function setComments(Collection $comments)
    {
        $this->comments = $comments;
    }

    public function addComment(Comment $comment)
    {
        $this->comments->addItem($comment);
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date)
    {
        $this->date = $date;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getContent()
    {
        return $this->content;
    }

    public static function getStatusLabels()
    {
        return self::$statusLabels;
    }

    public function getStatusLabel()
    {
        if (self::$statusLabels[$this->status] === null) {
            throw new Exception("Unknown status: " . $this->status);
        }
        return self::$statusLabels[$this->status];
    }
}