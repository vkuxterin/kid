<?php
require_once "UserAwareInterface.php";
class Comment implements UserAwareInterface {
    use UserAwareTrait;
    private $subject;
    private $date;
    private $content;
    function Comment(){
        settype($this->date,'DateTime');
        $this->date=new DateTime();
    }
    public function getSubject()
    {
        return $this->subject;
    }
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }
    public function getContent()
    {
        return $this->content;
    }
    public function setContent($content)
    {
        $this->content = $content;
    }
    public function getDate(): DateTime
    {
        return $this->date;
    }
    public function setDate(DateTime $date)
    {
        $this->date = $date;
    }
}