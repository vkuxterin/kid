<?php
interface UserAwareInterface{
    function getUser();
    function setUser(BaseUserInterface $user);

}