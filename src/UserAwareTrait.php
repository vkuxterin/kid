<?php
require_once "UserAwareInterface.php";
trait UserAwareTrait{
    private $user;

    public function getUser(){
        return $this->user;
    }
    public function setUser(BaseUserInterface $user){
        $this->user = $user;
    }
}